import React, { PureComponent } from 'react';
import ReactEcharts from 'echarts-for-react';
import echarts from 'echarts';
import {
    withStyles,
    Card,
    Paper,
    Icon,
    Divider,
    Typography,
    AppBar,
    Button,
    CardContent,
    IconButton,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Toolbar
} from "@material-ui/core";

export default class EchartScore extends PureComponent {
    render() {
        return (
            // <div >
            //     <div >
            //         <div className="p-16 pb-0 flex flex-row flex-wrap items-end">
            //             <div className=" headerOverview" style={{ justifyContent: "unset" }}>
            //                 <Typography className="text-56 font-300 leading-none valueOverview">
            //                     <Icon style={{ marginRight: "10px" }}>important_devices</Icon>
            //                 </Typography>
            //                 <Typography className="h3" color="textSecondary">
            //                     Device
            //              </Typography>
            //             </div>
            //         </div>
            // <div className="w-100-p mb-8">
            <ReactEcharts
                option={this.props.data}
                style={{ height: "100%" }}
            />
            // </div>
            //     </div>
            // </div>
        );
    }
};
