
var MysqlDB = require('../routes/connectDB');

module.exports = function checkExistGameLeaderboard(game, callback) {
    MysqlDB.query("select NameSpace from game where id = " + game.id, function (error, result) {
        if (error) {
            callback({
                status: "error",
                code: "400-s1",
                error: error
            });
        }
        else {
            try {
                var NameSpace = result[0].NameSpace;
                console.log(NameSpace);
                if (NameSpace) {
                    callback(NameSpace);
                }
                else {
                    callback({
                        status: "error-s3",
                        code: "404",
                        msg: "don't have this game"
                    })
                }
            } catch (e) {
                callback({
                    status: "error-s3",
                    code: "404",
                    msg: "don't have this game"
                })
            }
        }
    });
}