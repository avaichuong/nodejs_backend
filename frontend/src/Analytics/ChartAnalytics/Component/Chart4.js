import React, { useState, useEffect } from 'react';
import classNames from "classnames";
import {
    Button,
    ButtonGroup,
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Label,
    FormGroup,
    Input,
    Table,
    Row,
    Col,
    UncontrolledTooltip
} from "reactstrap";
import GraphChart1 from '../GraphComponent/GraphChart1';


export default function Chart4(props) {
    return (
        <>
            <Card className="card-chart">
                <CardHeader>
                    <CardTitle tag="h3">
                        <i className="tim-icons icon-send text-success" /> Tracking Game
                  </CardTitle>
                </CardHeader>
                <CardBody>
                    <div className="chart-area">
                        <GraphChart1 data={props.data} />
                    </div>
                </CardBody>
            </Card>
        </>
    )
}
