import React from "react";
import classNames from "classnames";
import { Line, Bar } from "react-chartjs-2";
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// core components
import {
  chartExample1,
  chartExample2,
  chartExample3,
  chartExample4
} from "variables/charts.jsx";
import PageView from './Component/PageView';
import PlayerOfGame from './Component/PlayerOfGame';
import GamePerMonth from './Component/GamePerMonth';
import Score from './Component/Score';
import { DashboardProvider } from './contextDashboard/context';

function Dashboard() {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     bigChartData: "data1"
  //   };
  // }
  // setBgChartData = name => {
  //   this.setState({
  //     bigChartData: name
  //   });
  // };
  return (
    <DashboardProvider>
      <>
        <div className="content">
          {/* start chart */}
          <PageView />
          <Row>
            <Col lg="4">
              <PlayerOfGame />
            </Col>
            <Col lg="4">
              <GamePerMonth />
            </Col>
            <Col lg="4">
              <Score />
            </Col>
          </Row>
          {/* end charts */}
        </div>
      </>
    </DashboardProvider>
  );
}

export default Dashboard;
