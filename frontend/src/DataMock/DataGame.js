var games = [
    {
        "ID": 1,
        "NameGame": "Soccer",
        "Icon": "icon game",
        "Config": "config",
        "Description": "No des",
        "NameSpace": "theSpace",
        "user_ID": 4
    },
    {
        "ID": 2,
        "NameGame": "Basketball",
        "Icon": "icon game",
        "Config": "config",
        "Description": "No des",
        "NameSpace": "basketball",
        "user_ID": 4
    },
    {
        "ID": 3,
        "NameGame": "Catch Fish",
        "Icon": "icon game",
        "Config": "config",
        "Description": "No des",
        "NameSpace": "catchFish",
        "user_ID": 4
    },
    {
        "ID": 4,
        "NameGame": "Save Princess",
        "Icon": "icon game",
        "Config": "config",
        "Description": "No des",
        "NameSpace": "savePrincess",
        "user_ID": 4
    }
]

export default games;