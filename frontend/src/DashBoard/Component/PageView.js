import React, { useState, useEffect } from 'react';
import classNames from "classnames";
import {
    Button,
    ButtonGroup,
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Label,
    FormGroup,
    Input,
    Table,
    Row,
    Col,
    UncontrolledTooltip
} from "reactstrap";
import GraphPageView from '../GraphComponent/GraphPageView';


export default function PageView() {

    return (
        <div>
            <Row>
                <Col xs="12">
                    <Card className="card-chart">
                        <CardHeader>
                            <Row>
                                <Col className="text-left" sm="6">
                                    <CardTitle tag="h2">Page View</CardTitle>
                                </Col>
                            </Row>
                        </CardHeader>
                        <CardBody>
                            <div className="chart-area">
                                <GraphPageView />
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}
