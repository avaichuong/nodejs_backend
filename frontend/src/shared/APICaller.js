
import axios from 'axios';

export function getAPIWithoutToken(strAPI, callback) {
    axios.defaults.headers["Content-Type"] =
        "application/x-www-form-urlencoded";

    axios.get(strAPI).then(_res => {
        callback(_res);
    })
        .catch(function (error) {
            console.log(error);
            callback([]);
        });

}