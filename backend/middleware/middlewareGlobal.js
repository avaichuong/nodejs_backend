var jwt = require('jsonwebtoken');
var CryptoJS = require('crypto-js');
var key = require('../routes/secretKey');

var pathURL = [
    "login",
    "register",
    'pub',
    'player-login'
];

function AllowPath(path) {
    for (let i in pathURL) {
        if (path.includes(pathURL[i])) {
            return true;
        }
    }
    return false;
}

function MiddlewareGlobal() {
    //verify token 
    this.verifyToken = function (req, res, next) {
        //Get auth header value
        if (AllowPath(req.path)) {
            next();
        }
        else {
            const bearerHeader = req.headers['authorization'];
            if (typeof bearerHeader !== undefined) {
                //split at the space

                const bearer = bearerHeader.split(' ');

                const bearerToken = bearer[1];

                req.token = bearerToken;
                jwt.verify(bearerToken, key, function (err, authData) {
                    if (err) {
                        res.sendStatus(403);
                    }
                    else {
                        console.log(authData);
                        req.user = authData.user;
                        next();
                    }
                });

            }
            else {
                // send 403
                res.sendStatus(403);
            }
        }

        // // console.log(key);
        // var ciphertext = CryptoJS.AES.encrypt("My password",key);
        // console.log(ciphertext.toString());
        // var bytes = CryptoJS.AES.decrypt(ciphertext.toString(),key);
        // var password = bytes.toString(CryptoJS.enc.Utf8);
        // console.log("hello this is middleware", password);
        // next();

    }

    //check token for user




}


module.exports = MiddlewareGlobal;