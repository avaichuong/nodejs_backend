
var MysqlDB = require('../routes/connectDB');
var CryptoJS = require('crypto-js');
var key = require('../routes/secretKey');


function UserDAO() {

    this.getUserModel = function (callback) {
        var sql = "select * from user";
        MysqlDB.query(sql, function (error, results) {
            if (error) {
                callback({
                    "status": "error",
                    "code": "400",
                    "error": error
                });
            }
            else {
                callback(results);
            }
        });
    }

    this.getUserByNameModel = function (user, callback) {
        var sql = "select * from user where UserName = ?";
        MysqlDB.query(sql, [user.UserName], function (error, results) {
            if (error) {
                callback({
                    "status": "error",
                    "code": "400",
                    "error": error
                });
            }
            else {
                callback(results);
            }
        });
    }

    this.postUserModel = function (user, callback) {
        var pass = CryptoJS.AES.encrypt(user.Password, key).toString();

        var sql = "insert into user (UserName, Email, Password) values (?,?,?)";
        MysqlDB.query(sql, [user.UserName, user.Email, pass], function (error, results) {
            if (error) {
                callback({
                    "status": "error",
                    "code": "400",
                    "error": error
                });
            }
            else {
                callback({
                    "status": "ok",
                    "code": "200"
                });
            }
        });

    }

    this.updateUserModel = function (user,ID, callback) {
        var pass = CryptoJS.AES.encrypt(user.Password, key).toString();
        var sql = "UPDATE user SET UserName = ?, Email = ?, Password = ? WHERE ID = ?";

        MysqlDB.query(sql, [user.UserName,user.Email,pass,ID], function (error, results) {
            if (error) {
                callback({
                    "status": "error",
                    "code": "400",
                    "error": error
                });
            }
            else {
                callback({
                    "status": "ok",
                    "code": "200"
                });
            }
        });

    }

}


module.exports = {
    UserDAO: UserDAO
}