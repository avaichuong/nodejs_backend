import React from "react";
import { useImmer } from "use-immer";

const defaultState = {
  
};

const DashboardContext = React.createContext();

const DashboardProvider = ({ children }) => {
  const [state, dispatch] = useImmer({ ...defaultState });

  return (
    <DashboardContext.Provider value={[state, dispatch]}>
      {children}
    </DashboardContext.Provider>
  );
};

export { DashboardProvider, DashboardContext };
