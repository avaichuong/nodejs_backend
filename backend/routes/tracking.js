
var TrackingDAO = require('../model/tracking').TrackingDAO;

function Tracking() {

    var TrackingModel = new TrackingDAO();

    this.getTracking = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        TrackingModel.getTrackingModel(game, function (result) {
            res.json(result);
        });

    }

    this.postTracking = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var TrackingInfo = {
            PlayerID: req.body.PlayerID,
            PlayerName: req.body.PlayerName,
            Data: req.body.Data
        }

        TrackingModel.postTrackingModel(game, TrackingInfo, function (result) {
            res.json(result);
        });

    }

    this.updateTracking = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var TrackingInfo = {
            PlayerID: req.body.PlayerID,
            PlayerName: req.body.PlayerName,
            Data: req.body.Data,
            TrackingID: req.body.TrackingID
        }

        TrackingModel.updateTrackingModel(game, TrackingInfo, function (result) {
            res.json(result);
        })

    }

    this.deleteTracking = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var TrackingInfo = {
            TrackingID: req.body.TrackingID
        }

        TrackingModel.deleteTrackingModel(game, TrackingInfo, function (result) {
            res.json(result);
        })

    }

}

module.exports = Tracking;