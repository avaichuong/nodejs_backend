
var Action = ["View", "Play", "Finish", "Ads", "CTA"];
var PlayerName = ["PhucTran", "Hello", "AD_none", "MiuMiu", "AAA"];

var tmpData = [];

for (let i = 0; i < 1000; i++) {

    let keyPlayerName = Math.floor(Math.random() * 5);
    let keyAction = Math.floor(Math.random() * 5);

    var d = new Date();
    var n = (new Date(d.getTime() + 15 * 24 * 60 * 60 * 1000 / 500 * i - 28 * 24 * 60 * 60 * 1000)).toLocaleDateString() + " " + (new Date(d.getTime() + 4320000 * i - 864000000)).toLocaleTimeString();

    tmpData.push({
        ID: i,
        PlayerID: keyAction,
        PlayerName: PlayerName[keyPlayerName],
        Data: Action[keyAction],
        Timestamp: n
    });

}

export default tmpData;