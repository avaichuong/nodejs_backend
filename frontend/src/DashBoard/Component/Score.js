import React, { useState, useEffect } from 'react';
import classNames from "classnames";
import {
    Button,
    ButtonGroup,
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Label,
    FormGroup,
    Input,
    Table,
    Row,
    Col,
    UncontrolledTooltip
} from "reactstrap";
import GraphScore from '../GraphComponent/GraphScore';


export default function Score() {
    return (
        <>
            <Card className="card-chart">
                <CardHeader>
                    <h5 className="card-category">Score of Game</h5>
                    <CardTitle tag="h3">
                        <i className="tim-icons icon-send text-success" /> 12,100K
                  </CardTitle>
                </CardHeader>
                <CardBody>
                    <div className="chart-area">
                        <GraphScore />
                    </div>
                </CardBody>
            </Card>
        </>
    )
}
