
var MysqlDB = require('../routes/connectDB');
var checkExist = require('./checkExistGameTable');

function TrackingDAO() {

    this.getTrackingModel = function (game, callback) {
        checkExist(game, function (_nameSpace) {

            var sql = "select * from Game_" + _nameSpace + "_Tracking";
            MysqlDB.query(sql, function (error, result) {
                if (error) {
                    callback({
                        status: "error-s2",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            })

        });
    }

    this.postTrackingModel = function (game, TrackingInfo, callback) {

        checkExist(game, function (_nameSpace) {

            var sql = "INSERT INTO `db_game`.`Game_" + _nameSpace + "_Tracking` (`PlayerID`, `PlayerName`, `Data`, `Timestamp`) VALUES (?, ?, ?,?)";

            MysqlDB.query(sql, [TrackingInfo.PlayerID, TrackingInfo.PlayerName, TrackingInfo.Data, (new Date()).toLocaleDateString() + ' ' + (new Date()).toLocaleTimeString()], function (error, result) {

                if (error) {
                    callback({
                        status: "error-s2",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }

            })

        })


    }


    this.updateTrackingModel = function (game, TrackingInfo, callback) {


        checkExist(game, function (_nameSpace) {

            var sql = "UPDATE `db_game`.`Game_" + _nameSpace + "_Tracking` SET `PlayerID`=?, `PlayerName`=?, `Data`=? WHERE `ID`=?";

            MysqlDB.query(sql, [TrackingInfo.PlayerID, TrackingInfo.PlayerName, TrackingInfo.Data, TrackingInfo.TrackingID], function (error, result) {
                if (error) {
                    callback({
                        status: "error-s2",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            });


        });

    }

    this.deleteTrackingModel = function (game, TrackingInfo, callback) {
        checkExist(game, function (_nameSpace) {
            var sql = "delete from `db_game`.`Game_" + _nameSpace + "_Tracking` where ID = ?";

            MysqlDB.query(sql, [TrackingInfo.TrackingID], function (error, result) {
                if (error) {
                    callback({
                        status: "error-s2",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            });

        })
    }


}

module.exports = {
    TrackingDAO
}