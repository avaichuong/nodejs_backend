
var PlayerDAO = require('../model/player').PlayerDAO;

function Player() {

    var PlayerModel = new PlayerDAO();

    this.getPlayer = function (req, res, next) {

        var game = {
            id: req.params.id
        }

        PlayerModel.getPlayerModel(game, function (result) {
            res.json(result);
        });


    }

    this.postPlayer = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var PlayerInfo = {
            PlayerName: req.body.PlayerName,
            Email: req.body.Email,
            Password: req.body.Password
        }

        PlayerModel.postPlayerModel(game, PlayerInfo, function (result) {
            res.json(result);
        })

    }

    this.updatePlayer = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var PlayerInfo = {
            PlayerName: req.body.PlayerName,
            Email: req.body.Email,
            Password: req.body.Password
        }


        PlayerModel.updatePlayerModel(game, PlayerInfo, function (result) {
            res.json(result);
        });


    }

    this.deletePlayer = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var PlayerInfo = {
            ID: req.body.ID
        }

        PlayerModel.deletePlayerModel(game, PlayerInfo, function (result) {
            res.json(result);
        })

    }

    this.login = function (req, res, next) {
        var PlayerInfo = {
            Email: req.body.Email,
            Password: req.body.Password
        }
        
        var game = {
            id: req.params.id
        }

        PlayerModel.loginModel(game, PlayerInfo, function (result) {
            res.json(result);
        });

    }

}


module.exports = Player;