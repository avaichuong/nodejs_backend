import React from 'react';
import {
    Paper,
    Grid
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Chart1 from './Component/Chart1';
import Chart2 from './Component/Chart2';
import Chart3 from './Component/Chart3';
import Chart4 from './Component/Chart4';

import data from '../../DataMock/Data';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        border: "none",
        background: "transparent",
        textAlign: "center",
        boxShadow: "none",
        display: "flex",
        justifyContent: "space-between"
    },
}));

export default function ChartAnalytics() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12} lg={3} md={6}>
                    <Paper className={classes.paper}>
                        <Chart1 data={data} />
                    </Paper>
                </Grid>
                <Grid item xs={12} lg={3} md={6}>
                    <Paper className={classes.paper}>
                        <Chart2 data={data} />
                    </Paper>
                </Grid>
                <Grid item xs={12} lg={3} md={6}>
                    <Paper className={classes.paper}>
                        <Chart3 data={data} />
                    </Paper>
                </Grid>
                <Grid item xs={12} lg={3} md={6}>
                    <Paper className={classes.paper}>
                        <Chart4 data={data} />
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}
