import React, { useEffect, useState } from "react";
import EchartComponent1 from '../EchartComponent/EchartComponent1';
import echarts from 'echarts';
import data from '../../../DataMock/Tool';


// Define chart
var option = {
    xAxis: {
        type: 'category',
        data: ["View", "Play", "Finish"],
        axisLabel: {
            textStyle: {
                color: "#ffffff"
            }
        },
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            textStyle: {
                color: "#ffffff"
            }
        },
    },
    tooltip: {
        // trigger: 'axis',
        axisPointer: {
            // type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    grid: {
        x: 50,
        y: 8,
        x2: 20,
        y2: 35,
    },
    series: [{
        data: [],
        type: 'bar',
        color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgb(255, 158, 68)'
        }, {
            offset: 1,
            color: 'rgb(255, 70, 131)'
        }])

    }]
};


export default function GraphChart1(props) {

    // const [data, setData] = useState();
    const [loading, setLoading] = useState(true);

    useEffect(() => {

        var numView = 0;
        var numPlay = 0;
        var numFinish = 0;

        for (let i = 0; i < data.length; i++) {
            if (data[i].Data == 'View') {
                numView++;
            }
            else if (data[i].Data == 'Play') {
                numPlay++;
            }
            else if (data[i].Data == 'Finish') {
                numFinish++;
            }
            else {

            }
        }

        option.series[0].data = [...[numView, numPlay, numFinish]];

        console.log(option);
        setLoading(false);



    }, [])

    return (
        <>
            {loading ? <>Loading...</> :
                <>
                    <EchartComponent1 data={option} />
                </>
            }
        </>
    )
}
