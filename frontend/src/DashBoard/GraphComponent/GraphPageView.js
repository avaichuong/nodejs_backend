import React, { useEffect, useState } from "react";
import EchartPageView from '../ChartComponent/EchartPageView';
import echarts from 'echarts';
import data from '../../DataMock/Tool';
import ReactEcharts from 'echarts-for-react';
import { Button } from "@material-ui/core";


// Define chart
var EchartChart = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            // type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        axisLabel: {
            textStyle: {
                color: "#ffffff"
            }
        },
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            textStyle: {
                color: "#ffffff"
            }
        },
    },
    grid: {
        x: 50,
        y: 8,
        x2: 20,
        y2: 45,
    },
    series: [{
        data: [820, 932, 901, 934, 1290, 1330, 1320],
        type: 'line',
        color: "#2d66f5",
        areaStyle: {
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#1d8bf7'
            }, {
                offset: 1,
                color: '#2d66f5'
            }]),
            opacity: 0.8
        },
        smooth: true
    }]
};

var Label_one_day = []

function checkExist(value, arr) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].Timestamp.substring(0, 10).includes(value)) {
            return i;
        }
    }
    return -1;
}

function checkExistHour(value, arr) {
    for (let i = 0; i < arr.length; i++) {
        // console.log(arr[i].Timestamp);
        if ((new Date(value)).getHours().toString() == (new Date(arr[i].Timestamp)).getHours().toString()) {
            return i;
        }
    }
    return -1;
}

function getDataByNumberDay(numberDay, data) {
    var tmpDay = new Date((new Date()).getTime() - numberDay * 24 * 60 * 60 * 1000);
    var dayString = tmpDay.toLocaleDateString();
    console.log(dayString);

    var mark_point = -1;

    for (let i = 0; i < data.length; i++) {
        if (data[i].Timestamp.substring(0, 10).includes(dayString)) {
            console.log(data[i]);
            mark_point = i;
            break;
        }
    }

    console.log(mark_point);

    if (mark_point >= 0) {
        var data_28 = data.slice(mark_point, data.length - 1);
        console.log(data_28);

        var result = [];

        for (let i = 0; i < data_28.length; i++) {
            // var point = checkExist(data_28[i].Timestamp.substring(0, 9), result);
            // console.log(data_28[i].Timestamp.substring(0, 10));
            // console.log(checkExist(data_28[i].Timestamp.substring(0, 10), data_28));
            if (checkExist(data_28[i].Timestamp.substring(0, 10), result) === -1) {
                result.push({
                    Timestamp: data_28[i].Timestamp,
                    value: 1
                });
            }
            else {
                result[checkExist(data_28[i].Timestamp.substring(0, 10), result)].value++;
            }
        }

        return result;
    }
    else {
        return [];
    }
}


function getDataByToDay(data) {
    var tmpDay = new Date();
    var dayString = tmpDay.toLocaleDateString();
    var result = [];
    for (let i = 0; i < data.length; i++) {
        if (data[i].Timestamp.substring(0, 10).includes(dayString)) {
            result.push({
                Timestamp: data[i].Timestamp,
                value: 1
            });
        }
    }

    result.sort((a, b) => {
        var a_date = new Date(a.Timestamp);
        var b_date = new Date(b.Timestamp);
        return a_date - b_date;
    });

    console.log(result);

    var resultHour = [];

    for (let i = 0; i < result.length; i++) {
        if (checkExistHour(result[i].Timestamp, resultHour) === -1) {
            console.log(result[i].Timestamp);
            resultHour.push({
                Timestamp: result[i].Timestamp,
                value: 1
            })
        }
        else {
            resultHour[checkExistHour(result[i].Timestamp, resultHour)].value++;
        }
    }

    console.log(resultHour);



    return resultHour;
}

export default function GraphPageView() {

    // const [data, setData] = useState();
    const [loading, setLoading] = useState(true);
    const [sum, setSum] = useState();
    const [size, setSize] = useState(10);
    const [data_28_day, setData_28_day] = useState();
    const [data_to_day, setData_to_day] = useState();
    const [data_7_day, setData_7_day] = useState();

    useEffect(() => {

        var result = getDataByToDay(data);
        console.log(result);
        setLoading(true);

        if (result.length > 0) {
            EchartChart.xAxis.data = result.map((item) => {
                return item.Timestamp;
            })

            EchartChart.series[0].data = result.map((item) => {
                return item.value;
            })

            console.log(EchartChart);
            setData_28_day(EchartChart);
            setLoading(false);
        }

    }, [])

    // console.log(data);

    function handleClickGetData_28_day() {

        var result = getDataByNumberDay(28, data);
        // console.log(result);
        setLoading(true);

        setTimeout(() => {
            if (result.length > 0) {
                EchartChart.xAxis.data = result.map((item) => {
                    return item.Timestamp;
                })

                EchartChart.series[0].data = result.map((item) => {
                    return item.value;
                })

                console.log(EchartChart);
                setData_28_day(EchartChart);
                setLoading(false);
            }
        })

    }

    function handleClickGetData_To_day() {

        setLoading(true);
        var result = getDataByToDay(data);
        console.log(result);
        setTimeout(() => {
            if (result.length > 0) {
                EchartChart.xAxis.data = result.map((item) => {
                    return item.Timestamp;
                })

                EchartChart.series[0].data = result.map((item) => {
                    return item.value;
                })

                console.log(EchartChart);
                setData_28_day(EchartChart);
                setLoading(false);
            }
        })

    }

    function handleClickGetData_7_day() {
        var result = getDataByNumberDay(7, data);
        // console.log(result);
        setLoading(true);

        setTimeout(() => {
            if (result.length > 0) {
                EchartChart.xAxis.data = result.map((item) => {
                    return item.Timestamp;
                })

                EchartChart.series[0].data = result.map((item) => {
                    return item.value;
                })

                console.log(EchartChart);
                setData_28_day(EchartChart);
                setLoading(false);
            }
        })

    }

    return (
        <>
            {loading ? <>Loading...</> :
                <>
                    {console.log(data_28_day)}
                    <div className="dashboard__choose-show-data" >
                        <Button className="buttonStyle" onClick={handleClickGetData_28_day} >28 day</Button>
                        <Button onClick={handleClickGetData_To_day} >1 day</Button>
                        <Button onClick={handleClickGetData_7_day} >7 day</Button>
                    </div>
                    {/* <EchartPageView data={data_28_day ? data_28_day : EchartChart} /> */}

                    <ReactEcharts
                        option={data_28_day}
                        style={{ height: "100%" }}
                    />
                </>
            }
        </>
    )
}
