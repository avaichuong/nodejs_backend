var LoginDAO = require('../model/login').LoginDAO;
var jwt = require('jsonwebtoken');
var key = require('./secretKey');

function Login() {

    var LoginModel = new LoginDAO();

    this.postLogin = function (req, res, next) {

        var user = req.body;

        LoginModel.postLogin(user, function (result) {
            if (result.status === "ok") {
                jwt.sign({ user: result.result }, key, { expiresIn: '1h' }, function (error, token) {
                    res.json({
                        token: token
                    })
                });
            }
            else {
                res.json(result);
            }
        });

    }

}


module.exports = Login;