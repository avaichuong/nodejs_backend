var express = require('express');
var router = express.Router();

//connect controller
var GameHandler = require('./game');
var UserHandler = require('./user');
var LoginHandler = require('./login');
var LeaderboardHandler = require('./leaderboard');
var PlayerHandler = require('./player');
var TrackingHandler = require('./tracking');

//create object controller
var Game = new GameHandler();
var User = new UserHandler();
var Login = new LoginHandler();
var Leaderboard = new LeaderboardHandler();
var Player = new PlayerHandler();
var Tracking = new TrackingHandler();



//Global Middleware

var MiddlewareGlobal = require("../middleware/middlewareGlobal");
var checkGlobal = new MiddlewareGlobal();
router.use(checkGlobal.verifyToken);

//end Global Middleware

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});


//test
router.get('/test', function (req, res, next) {
  var sql = "select * from game";
  MysqlDB.query(sql, function (error, results) {
    if (error) {
      res.send({
        "status": "error",
        "code": "400",
        "error": error
      });
    } else {
      res.send(results);
    }
  });
});


// API for table game

router.get('/game', Game.getGame);
router.post('/game/new', Game.postNewGame);


// API for user
router.get('/user', User.getUser);
router.get('/user/profile', User.getUserByName);
router.post('/register', User.postUser);
router.put('/user/update', User.updateUser);


//API for login
router.post('/admin-game/login', Login.postLogin);


//API for  Game Leaderboard Table --- all score table
router.get('/pub/player-get-leaderboard/:id', Leaderboard.getLeaderboard);
router.post('/pub/player-post-leaderboard/:id', Leaderboard.postLeaderboad);
router.put('/pub/player-update-leaderboard/:id', Leaderboard.updateLeaderboard);
router.delete('/pub/player-delete-leaderboard/:id', Leaderboard.deleteLeaderboard);


//API for Game Player Table
router.get('/pub/player-get-player/:id', Player.getPlayer);
router.post('/pub/player-post-player/:id', Player.postPlayer);
router.put('/pub/player-update-player/:id', Player.updatePlayer);
router.delete('/pub/player-delete-player/:id', Player.deletePlayer);
router.post('/player-login/:id', Player.login);


//API for Game Tracking Table
router.get('/pub/player-get-tracking/:id', Tracking.getTracking);
router.post('/pub/player-post-tracking/:id', Tracking.postTracking);
router.put('/pub/player-update-tracking/:id', Tracking.updateTracking);
router.delete('/pub/player-delete-tracking/:id', Tracking.deleteTracking);


module.exports = router;
