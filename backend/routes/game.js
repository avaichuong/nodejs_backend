
// var MysqlDB = require('./connectDB');
var GameDAO = require('../model/game').GameDAO;

function Game() {

    var GameModel = new GameDAO();

    this.getGame = function (req, res, next) {
        GameModel.getGameModel(function (results) {
            res.json(results);
        });
    }

    this.postNewGame = function (req, res, next) {

        var game = {
            NameGame: req.body.NameGame,
            Icon: req.body.Icon,
            Config: req.body.Config,
            Description: req.body.Description,
            NameSpace: req.body.NameSpace,
            user_ID: req.user.ID
        }

        GameModel.postGameModel(game, function (results) {
            res.json(results);
        })
    };

}

module.exports = Game;