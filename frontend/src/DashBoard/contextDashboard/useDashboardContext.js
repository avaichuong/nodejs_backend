import { useContext, useEffect } from "react";

import { DashboardContext } from "./context";


const useDashboardContext = () => {
    const [state, dispatch] = useContext(DashboardContext);

    useEffect(() => {


    }, [])

    return {
        ...state
    };
};

export { useDashboardContext };
