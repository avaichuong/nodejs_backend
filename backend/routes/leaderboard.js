
var LeaderBoardDAO = require('../model/leaderboard').LeaderboardDAO;

function LeaderBoard() {

    var LeaderBoardModel = new LeaderBoardDAO();

    this.getLeaderboard = function (req, res, next) {

        var game = { id: req.params.id }

        LeaderBoardModel.getLeaderboardModel(game, function (_res) {
            res.json(_res);
        })

    }

    this.postLeaderboad = function (req, res, next) {
        var game = {
            id: req.params.id
        };

        var leaderInfo = {
            playerID: req.body.playerID,
            playerName: req.body.playerName,
            playerScore: req.body.playerScore
        }

        console.log(leaderInfo);

        LeaderBoardModel.postLeaderboardModel(game, leaderInfo, function (_res) {
            res.json(_res);
        })

    }

    this.updateLeaderboard = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var leaderInfo = {
            playerID: req.body.playerID,
            playerName: req.body.playerName,
            playerScore: req.body.playerScore
        }

        LeaderBoardModel.updateLeaderboardModel(game, leaderInfo, function (result) {
            res.json(result);
        })

    }

    this.deleteLeaderboard = function (req, res, next) {
        var game = {
            id: req.params.id
        }

        var leaderInfo = {
            playerID: req.body.playerID,
            playerName: req.body.playerName,
            playerScore: req.body.playerScore
        }

        LeaderBoardModel.deleteLeaderboardModel(game, leaderInfo, function (result) {
            res.json(result);
        })
    }

}

module.exports = LeaderBoard;