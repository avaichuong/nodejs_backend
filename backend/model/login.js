
var MysqlDB = require('../routes/connectDB');
var CryptoJS = require('crypto-js');
var key = require('../routes/secretKey');

function LoginDAO() {

    this.postLogin = function (user, callback) {

        var sql = "select * from user where UserName = ?";
        MysqlDB.query(sql, [user.UserName, user.Password], function (error, results) {
            if (error) {
                callback({
                    "status": "error",
                    "code": "400"
                });
            }
            else {
                if (results.length !== 0) {
                    var bytes = CryptoJS.AES.decrypt(results[0].Password.toString(), key);
                    var password = bytes.toString(CryptoJS.enc.Utf8);

                    if (user.Password === password) {
                        callback({
                            "status": "ok",
                            "code": "200",
                            "result": results[0]
                        });
                    }
                    else {
                        callback({
                            "status": "error",
                            "code": "401"
                        });
                    }

                }
                else {
                    callback({
                        "status": "error",
                        "code": "401"
                    });
                }
            }
        });

    }

}


module.exports = {
    LoginDAO: LoginDAO
}