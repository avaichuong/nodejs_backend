var UserDAO = require('../model/user').UserDAO;


function User() {

    var UserModel = new UserDAO();
    this.getUser = function(req,res,next){
        UserModel.getUserModel(function(result){
            res.json(result);
        });
    }

    this.getUserByName = function(req,res,next){
        UserModel.getUserByNameModel(req.user,function(result){
            res.json(result);
        })
    }

    this.postUser = function(req,res,next){
        var user = req.body;
        UserModel.postUserModel(user,function(result){
            res.json(result);
        })
    }

    this.updateUser = function(req,res,next){
        var user = req.body;
        UserModel.updateUserModel(user,req.user.ID,function(result){
            res.json(result);
        })
    }


    
}

module.exports = User;