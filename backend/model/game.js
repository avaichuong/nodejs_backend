
var MysqlDB = require('../routes/connectDB');

function GameDAO() {
    this.getGameModel = function (callback) {
        var sql = "select * from game";
        MysqlDB.query(sql, function (error, results) {
            if (error) {
                res.send({
                    "status": "error",
                    "code": "400",
                    "error": error
                });
            } else {
                callback(results);
            }
        });
    }

    this.postGameModel = function (game, callback) {
        var sql = "insert into game (NameGame,Icon,Config,Description,NameSpace,user_ID) values (?,?,?,?,?,?)";
        MysqlDB.query(sql, [game.NameGame, game.Icon, game.Config, game.Description, game.NameSpace, game.user_ID], function (error, results) {
            if (error) {
                callback({
                    "status": "error",
                    "code": "400",
                    "error": error
                });
            }
            else {
                // callback({
                //     "status":"ok"
                // });

                var playerSql = "CREATE TABLE IF NOT EXISTS `db_game`.`Game_" + game.NameSpace + "_Player`" + "("
                    + "`ID` INT NOT NULL AUTO_INCREMENT,"
                    + "`PlayerName` VARCHAR(200) NOT NULL,"
                    + "`Email` VARCHAR(45) NOT NULL,"
                    + "`Password` VARCHAR(400) NOT NULL,"
                    + "PRIMARY KEY (`ID`),"
                    + "UNIQUE INDEX `ID_UNIQUE` (`ID` ASC),"
                    + "UNIQUE INDEX `PlayerName_UNIQUE` (`PlayerName` ASC),"
                    + "UNIQUE INDEX `Email_UNIQUE` (`Email` ASC))"
                    + "ENGINE = InnoDB";
                MysqlDB.query(playerSql, function (error, results) {
                    if (error) {
                        callback({
                            "status": "error",
                            "code": "400-createtable1",
                            "error": error
                        });
                    }
                    else {

                        var leaderBoardSql = "CREATE TABLE IF NOT EXISTS `db_game`.`Game_" + game.NameSpace + "_Leaderboard" + "` ("
                            + "`ID` INT NOT NULL AUTO_INCREMENT,"
                            + "`PlayerID` VARCHAR(200) NOT NULL,"
                            + "`PlayerName` VARCHAR(200) NOT NULL,"
                            + "`Score` INT(11) NULL DEFAULT 0,"
                            + "PRIMARY KEY (`ID`),"
                            + "UNIQUE INDEX `ID_UNIQUE` (`ID` ASC),"
                            + "UNIQUE INDEX `PlayerID_UNIQUE` (`PlayerID` ASC),"
                            + "UNIQUE INDEX `PlayerName_UNIQUE` (`PlayerName` ASC))"
                            + "ENGINE = InnoDB;";

                        MysqlDB.query(leaderBoardSql, function (error, results) {
                            if (error) {
                                callback({
                                    "status": "error",
                                    "code": "400-createtable2",
                                    "error": error
                                })
                            }
                            else {
                                var TrackingSql = "CREATE TABLE IF NOT EXISTS" + "`db_game`.`Game_" + game.NameSpace + "_Tracking" + "` ("
                                    + "`ID` INT NOT NULL AUTO_INCREMENT,"
                                    + "`PlayerID` INT(11) NOT NULL,"
                                    + "`PlayerName` VARCHAR(200) NOT NULL,"
                                    + "`Description` TEXT(2000) NULL,"
                                    + "PRIMARY KEY (`ID`),"
                                    + "UNIQUE INDEX `ID_UNIQUE` (`ID` ASC),"
                                    + "UNIQUE INDEX `PlayerID_UNIQUE` (`PlayerID` ASC),"
                                    + "UNIQUE INDEX `PlayerName_UNIQUE` (`PlayerName` ASC))"
                                    + "ENGINE = InnoDB;"
                                MysqlDB.query(TrackingSql, function (error, results) {
                                    if (error) {
                                        callback({
                                            "status": "error",
                                            "code": "400-createtable3",
                                            "error": error
                                        });
                                    }
                                    else {
                                        callback({
                                            "status": "ok"
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

}

module.exports = {
    GameDAO
}