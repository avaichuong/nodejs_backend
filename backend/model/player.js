

var MysqlDB = require('../routes/connectDB');
var checkExist = require('./checkExistGameTable');


function PlayerDAO() {

    this.getPlayerModel = function (game, callback) {

        checkExist(game, function (_nameSpace) {
            var sql = "select * from Game_" + _nameSpace + "_Player";
            MysqlDB.query(sql, function (error, result) {
                if (error) {
                    callback({
                        status: "error",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            });
        })

    }

    this.postPlayerModel = function (game, Player, callback) {
        checkExist(game, function (_nameSpace) {

            var sql = "INSERT INTO `db_game`.`Game_" + _nameSpace + "_Player` (`PlayerName`, `Email`, `Password`) VALUES ( ?, ?, ?)";
            MysqlDB.query(sql, [Player.PlayerName, Player.Email, Player.Password], function (error, result) {
                if (error) {
                    callback({
                        status: "error",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    var sql2 = "select * from `Game_" + _nameSpace + "_Player` where PlayerName = ?";
                    MysqlDB.query(sql2, [Player.PlayerName], function (error, result2) {
                        if (error) {
                            callback({
                                status: "error",
                                code: "400-s4",
                                error: error
                            });
                        }
                        else {
                            var sql2 = "INSERT INTO `db_game`.`Game_" + _nameSpace + "_Leaderboard` (`PlayerID`, `PlayerName`) VALUES (?, ?)";
                            MysqlDB.query(sql2, [result2[0].ID, result2[0].PlayerName], function (error, result3) {
                                if (error) {
                                    callback({
                                        status: "error",
                                        code: "400-s5",
                                        error: error
                                    })
                                }
                                else {
                                    callback({
                                        status: "ok",
                                        result: result3
                                    });
                                }
                            })
                        }
                    });
                }
            });

        });
    }


    this.updatePlayerModel = function (game, Player, callback) {
        checkExist(game, function (_nameSpace) {
            var sql = "UPDATE `db_game`.`Game_" + _nameSpace + "_Player` SET `PlayerName`=?, `Email`=?, `Password`=? WHERE `ID`=?";

            MysqlDB.query(sql, [Player.PlayerName, Player.Email, Player.Password], function (error, result) {callback({
                status: "ok",
                result: result
            })
                if (error) {
                    callback({
                        status: "error",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            });
        })
    }

    this.deletePlayerModel = function (game, Player, callback) {
        checkExist(game, function (_nameSpace) {

            var sql = "delete from `db_game`.`Game_" + _nameSpace + "_Player` where ID = ?";
            MysqlDB.query(sql, [Player.ID], function (error, result) {
                if (error) {
                    callback({
                        status: "error",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            });

        })
    }

    this.loginModel = function (game, Player, callback) {
        checkExist(game, function (_nameSpace) {
            var sql = "select * from `Game_" + _nameSpace + "_Player` where Email = ?";
            MysqlDB.query(sql, [Player.Email], function (error, result) {
                if (error) {
                    callback({
                        status: "error",
                        code: "403-s1",
                        error: error
                    });
                }
                else {
                    console.log(Player);
                    if (Player.Password === result[0].Password) {

                        var sql2 = "INSERT INTO `db_game`.`Game_theSpace_Tracking` (`PlayerID`, `PlayerName`, `Data`, `Timestamp`) VALUES (?, ?, 'View', ?)";

                        MysqlDB.query(sql2, [result[0].ID, result[0].PlayerName, (new Date()).toLocaleDateString() + " " + (new Date()).toLocaleTimeString()], function (error, resultTracking) {

                            if (error) {
                                callback({
                                    status: "error",
                                    code: "400-dont-add-to-tracking",
                                    error: error
                                })
                            }
                            else {
                                callback({
                                    status: "ok",
                                    result: result
                                })
                            }

                        });
                    }
                    else {
                        callback({
                            status: "error",
                            code: "403-s2",
                            error: error
                        });
                    }
                }
            });
        })
    }

}

module.exports = {
    PlayerDAO
}