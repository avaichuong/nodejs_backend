import React from 'react';
import GraphPlayerOfGame from '../GraphComponent/GraphPlayerOfGame';
import {
    Button,
    ButtonGroup,
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Label,
    FormGroup,
    Input,
    Table,
    Row,
    Col,
    UncontrolledTooltip
  } from "reactstrap";

export default function PlayerOfGame() {
    return (
        <>
            <Card className="card-chart">
                <CardHeader>
                    <h5 className="card-category">Player of game</h5>
                    <CardTitle tag="h3">
                        <i className="tim-icons icon-bell-55 text-info" />{" "}
                        763,215
                  </CardTitle>
                </CardHeader>
                <CardBody>
                    <div className="chart-area">
                        <GraphPlayerOfGame />
                    </div>
                </CardBody>
            </Card>

        </>
    )
}
