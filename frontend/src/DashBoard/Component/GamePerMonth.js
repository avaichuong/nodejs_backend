import React from 'react';
import classNames from "classnames";
import {
    Button,
    ButtonGroup,
    Card,
    CardHeader,
    CardBody,
    CardTitle,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    UncontrolledDropdown,
    Label,
    FormGroup,
    Input,
    Table,
    Row,
    Col,
    UncontrolledTooltip
} from "reactstrap";
import GraphGamePerMonth from '../GraphComponent/GraphGamePerMonth';


export default function GamePerMonth() {
    return (
        <>
            <Card className="card-chart">
                <CardHeader>
                    <h5 className="card-category">Game per month</h5>
                    <CardTitle tag="h3">
                        <i className="tim-icons icon-delivery-fast text-primary" />{" "}
                        3,500€
                  </CardTitle>
                </CardHeader>
                <CardBody>
                    <div className="chart-area">
                        <GraphGamePerMonth />
                    </div>
                </CardBody>
            </Card>
        </>
    )
}
