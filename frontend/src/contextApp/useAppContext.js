import { useContext, useEffect } from "react";

import { AppContext } from "./context";
import { getAPIWithoutToken } from "../shared/APICaller";


const useAppContext = () => {
    const [state, dispatch] = useContext(AppContext);

    useEffect(() => {

        getAPIWithoutToken("/pub/player-get-tracking/1", function (res) {
            console.log(res);
        })
        console.log("hello");


    }, [])

    function updateData(data) {
        dispatch((draft) => {
            draft.data = data;
        })
    }

    return {
        ...state,
        updateData

    };
};

export { useAppContext };
