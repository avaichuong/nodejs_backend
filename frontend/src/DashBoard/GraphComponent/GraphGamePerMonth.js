import React, { useEffect, useState } from "react";
import EchartPageView from '../ChartComponent/EchartPlayerOfGame';
import echarts from 'echarts';


// Define chart
var EchartChart = {
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        axisLabel: {
            textStyle: {
                color: "#ffffff"
            }
        },
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            textStyle: {
                color: "#ffffff"
            }
        },
    },
    grid: {
        x: 50,
        y: 8,
        x2: 20,
        y2: 35,
    },
    series: [{
        data: [820, 932, 901, 934, 1290, 1330, 1320],
        type: 'line',
        areaStyle: {
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                offset: 0,
                color: '#090979'
            }, {
                offset: 1,
                color: '#00fff4'
            }])
        },
        smooth: true
    }]
};

export default function GraphGamePerMonth() {

    const [data, setData] = useState();
    const [loading, setLoading] = useState(true);
    const [sum, setSum] = useState();
    const [size, setSize] = useState(10);

    useEffect(() => {

    }, [])

    console.log(data);

    return (
        <>
            {/* {loading ? <>Loading...</> : */}
            <>
                <EchartPageView data={EchartChart} />
            </>
            {/* } */}
        </>
    )
}
