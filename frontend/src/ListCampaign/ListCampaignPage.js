import React from 'react';
import {
    Paper,
    Grid
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Table from './TableCampaign/TableCampaign';
import NewCampaign from './Component/NewCampaignButton';
import SelectCampaign from './Component/Select';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        border: "none",
        background: "transparent",
        textAlign: "center",
        boxShadow: "none",
        display: "flex",
        justifyContent: "space-between"
    },
}));

export default function ListCampaignPage() {
    const classes = useStyles();
    return (
        <div className="content">
            <Grid container spacing={1}>
                <Grid item xs={12} >
                    <Paper className={classes.paper}>
                        <SelectCampaign />
                        <NewCampaign />
                    </Paper>
                </Grid>
            </Grid>
            <div className="listcampaign__table" >
                <Table />
            </div>
        </div>
    )
}
