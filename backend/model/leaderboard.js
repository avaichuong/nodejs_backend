
var MysqlDB = require('../routes/connectDB');
var checkExist = require('./checkExistGameTable');

function LeaderboardDAO() {


    this.getLeaderboardModel = function (game, callback) {

        checkExist(game, function (_nameSpace) {

            var sql = "select * from Game_" + _nameSpace + "_Leaderboard";
            MysqlDB.query(sql, function (error, resultLeaderboard) {
                if (error) {
                    callback({
                        status: "error-s2",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: resultLeaderboard
                    });
                }
            });

        })

    }


    this.postLeaderboardModel = function (game, leaderInfo, callback) {

        checkExist(game, function (_nameSpace) {
            var sql = "INSERT INTO `db_game`.`Game_" + _nameSpace + "_Leaderboard` (`PlayerID`, `PlayerName`, `Score`) VALUES (?, ?, ?)";
            MysqlDB.query(sql, [leaderInfo.playerID, leaderInfo.playerName, leaderInfo.playerScore], function (error, result) {
                if (error) {
                    callback({
                        status: "error-s2",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            });
        })

    }

    this.updateLeaderboardModel = function (game, leaderInfo, callback) {

        checkExist(game, function (_nameSpace) {
            var sql = "UPDATE `db_game`.`Game_" + _nameSpace + "_Leaderboard` SET `Score` = ? WHERE `ID` = ? and Game_theSpace_Leaderboard.Score<" + leaderInfo.playerScore;
            MysqlDB(sql, [leaderInfo.playerScore, leaderInfo.playerID], function (error, result) {
                if (error) {
                    callback({
                        status: "error",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            })

        })

    }

    this.deleteLeaderboardModel = function (game, leaderInfo, callback) {

        checkExist(game, function (_nameSpace) {
            var sql = "delete from `db_game`.`Game_" + _nameSpace + "_Leaderboard` where ID = ?";
            MysqlDB(sql, leaderInfo.playerID, function (error, results) {
                if (error) {
                    callback({
                        status: "error",
                        code: "400-s2",
                        error: error
                    });
                }
                else {
                    callback({
                        status: "ok",
                        result: result
                    });
                }
            })
        });

    }


}


module.exports = {
    LeaderboardDAO
}